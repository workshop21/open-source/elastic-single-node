# Elastic Single Node Deployment

This project contains the yamls to create a single node elastic deployment including a pvc on storage class block-repl-low (rook) and a service that redirects traffic on ${subgroup}-elastic-db.${k8s_namespace}.svc.cluster.local on port 9200 and 9300 to the elastic pod.

## set-up

When deploying a new elastic the init container "volume-mount-hack" should be uncommented in order to configure the pv on the first startup. Otherwise permission problems on the directory may occur. This init-container can be removed after the first successful deployment.

## deploy locally

To run elastic locally the creation of the storageclass block-repl-low is mandatory. The already prepared yaml is located in the deployLocal folder and works with minikube.  

After the stroageclass yaml has been applied to the local minikube cluster the deploy.sh can be executed to generate the yaml files from the templates. Keep in mind that the deploy.sh requires several variables to generate the yaml files successfully.

## required Variables

To apply the generated yaml files a variable containing the kubeconfig is required. The structure of the variable is expected as following:  

`KUBECONFIGFILE_$k8s_namespace (e.g.: KUBECONFIGFILE_STAGING)`